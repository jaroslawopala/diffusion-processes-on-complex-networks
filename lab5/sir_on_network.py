import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import imageio
from os import listdir
import os
import pandas as pd


class PopulationOnLattice:

    def __init__(self, n=10, p=0.7, infected_0=None):
        """Initialize class"""
        self.p = p
        self.lattice = np.zeros((n, n))
        if not infected_0:
            infected_0 = (np.random.randint(0, self.lattice.shape[0]), np.random.randint(0, self.lattice.shape[1]))
        self.lattice[infected_0] = 2
        self.infected = [infected_0]
        self.removed = []

    def infect_neighbors(self):
        """Infect neighbors of infected agents"""
        infected = self.infected.copy()
        for i, j in infected:
            if self.lattice[i, j] == 2:
                if i > 0:
                    if self.lattice[i-1, j] == 0:
                        if np.random.random() <= self.p:
                            self.lattice[i-1, j] = 2
                            self.infected.append((i-1, j))
                if i+1 < self.lattice.shape[0]:
                    if self.lattice[i+1, j] == 0:
                        if np.random.random() <= self.p:
                            self.lattice[i+1, j] = 2
                            self.infected.append((i+1, j))
                if j > 0:
                    if self.lattice[i, j-1] == 0:
                        if np.random.random() <= self.p:
                            self.lattice[i, j-1] = 2
                            self.infected.append((i, j-1))
                if j+1 < self.lattice.shape[1]:
                    if self.lattice[i, j+1] == 0:
                        if np.random.random() <= self.p:
                            self.lattice[i, j+1] = 2
                            self.infected.append((i, j+1))
                self.lattice[i, j] = 1
                self.infected.remove((i, j))
                self.removed.append((i, j))


class PopulationOnGraph:

    def __init__(self, graph, p=0.7, infected_0=None):
        """Initialize class"""
        self.p = p
        self.graph = graph
        for node in self.graph.nodes:
            self.graph.nodes[node]['status'] = 0
        if not infected_0:
            infected_0 = np.random.choice(self.graph.nodes)
        self.graph.nodes[infected_0]['status'] = 2
        self.infected = [infected_0]
        self.removed = []

    def infect_neighbors(self):
        """Infect neighbors of infected agents"""
        infected = self.infected.copy()
        for node in infected:
            neighbors = self.graph.neighbors(node)
            for neighbor in neighbors:
                if self.graph.nodes[neighbor]['status'] == 0:
                    if np.random.random() <= self.p:
                        self.graph.nodes[neighbor]['status'] = 2
                        self.infected.append(neighbor)
            self.infected.remove(node)
            self.graph.nodes[node]['status'] = 1
            self.removed.append(node)


def create_gif(path_to_images, path_to_result):
    """ Create gif from images.

    :param path_to_images: path to directory containing images
    :param path_to_result: path to directory to place the result
    """
    with imageio.get_writer(path_to_result, mode='I', fps=2) as writer:
        for filename in listdir(path_to_images):
            image = imageio.imread(path_to_images + filename)
            writer.append_data(image)


if __name__ == '__main__':
    np.random.seed(42)

    # Visualization
    p = 0.6

    for directory_name in ['lattice', 'random_graph', 'watts_strogatz_graph', 'barabasi_albert_graph']:
        path = os.path.join(os.getcwd(), 'results/exercise_3/' + directory_name)
        files = os.listdir(path)
        for f in files:
            os.remove(os.path.join(path, f))

    # Lattice
    population = PopulationOnLattice(p=p)
    i = 0
    while population.infected:
        im = plt.imshow(population.lattice, cmap=plt.cm.get_cmap('seismic'))
        population.infect_neighbors()
        plt.savefig('results/exercise_3/lattice/pic_{}'.format(100+i))
        i += 1
    plt.close()
    create_gif('results/exercise_3/lattice/', 'results/exercise_3/lattice.gif')

    # Random graph
    graph = nx.gnp_random_graph(n=30, p=0.3)
    population_on_graph = PopulationOnGraph(graph, p=p)
    pos = nx.spring_layout(graph)
    i = 0
    while population_on_graph.infected:
        nx.draw(population_on_graph.graph, pos)
        nx.draw(population_on_graph.graph, pos, nodelist=population_on_graph.infected, node_color='red')
        nx.draw(population_on_graph.graph, pos, nodelist=population_on_graph.removed, node_color='green')
        population_on_graph.infect_neighbors()
        plt.savefig('results/exercise_3/random_graph/pic_{}'.format(100+i))
        i += 1
    plt.close()
    create_gif('results/exercise_3/random_graph/', 'results/exercise_3/random_graph.gif')

    # Watts-Strogatz graph
    graph = nx.watts_strogatz_graph(n=30, k=5, p=0.4)
    population_on_graph = PopulationOnGraph(graph, p=p)
    pos = nx.spring_layout(graph)
    i = 0
    while population_on_graph.infected:
        nx.draw(population_on_graph.graph, pos)
        nx.draw(population_on_graph.graph, pos, nodelist=population_on_graph.infected, node_color='red')
        nx.draw(population_on_graph.graph, pos, nodelist=population_on_graph.removed, node_color='green')
        population_on_graph.infect_neighbors()
        plt.savefig('results/exercise_3/watts_strogatz_graph/pic_{}'.format(100+i))
        i += 1
    plt.close()
    create_gif('results/exercise_3/watts_strogatz_graph/', 'results/exercise_3/watts_strogatz_graph.gif')

    # Barabasi-Albert graph
    graph = nx.barabasi_albert_graph(n=30, m=3)
    population_on_graph = PopulationOnGraph(graph, p=p)
    pos = nx.spring_layout(graph)
    i = 0
    while population_on_graph.infected:
        nx.draw(population_on_graph.graph, pos)
        nx.draw(population_on_graph.graph, pos, nodelist=population_on_graph.infected, node_color='red')
        nx.draw(population_on_graph.graph, pos, nodelist=population_on_graph.removed, node_color='green')
        population_on_graph.infect_neighbors()
        plt.savefig('results/exercise_3/barabasi_albert_graph/pic_{}'.format(100+i))
        i += 1
    plt.close()
    create_gif('results/exercise_3/barabasi_albert_graph/', 'results/exercise_3/barabasi_albert_graph.gif')

    # Count
    n = 100
    for p in [0.2, 0.5, 0.8]:
        fraction_infected_lattice_full = []
        fraction_infected_random_graph_full = []
        fraction_infected_ws_graph_full = []
        fraction_infected_ba_graph_full = []
        for i in range(100):
            # Lattice
            population = PopulationOnLattice(n=10, p=p, infected_0=(3, 3))
            fraction_infected_lattice = [len(population.infected)/n]
            for j in range(10):
                population.infect_neighbors()
                fraction_infected_lattice.append(len(population.infected)/n)
            fraction_infected_lattice_full.append(fraction_infected_lattice)

            # Random graph
            random_graph = nx.gnp_random_graph(n, p)
            population = PopulationOnGraph(random_graph, p=p, infected_0=0)
            fraction_infected_random_graph = [len(population.infected) / n]
            for j in range(10):
                population.infect_neighbors()
                fraction_infected_random_graph.append(len(population.infected)/n)
            fraction_infected_random_graph_full.append(fraction_infected_random_graph)

            # Watts Strogatz graph
            watts_strogatz_graph = nx.watts_strogatz_graph(n, 6, 0.4)
            population = PopulationOnGraph(watts_strogatz_graph, p=p, infected_0=0)
            fraction_infected_ws_graph = [len(population.infected) / n]
            for j in range(10):
                population.infect_neighbors()
                fraction_infected_ws_graph.append(len(population.infected) / n)
            fraction_infected_ws_graph_full.append(fraction_infected_ws_graph)

            # Barabasi Albert graph
            barabasi_albert_graph = nx.barabasi_albert_graph(n, 4)
            population = PopulationOnGraph(barabasi_albert_graph, p=p, infected_0=0)
            fraction_infected_ba_graph = [len(population.infected) / n]
            for j in range(10):
                population.infect_neighbors()
                fraction_infected_ba_graph.append(len(population.infected) / n)
            fraction_infected_ba_graph_full.append(fraction_infected_ba_graph)

        fraction_infected_lattice_full = np.array(fraction_infected_lattice_full)
        fraction_infected_random_graph_full = np.array(fraction_infected_random_graph_full)
        fraction_infected_ws_graph_full = np.array(fraction_infected_ws_graph_full)
        fraction_infected_ba_graph_full = np.array(fraction_infected_ba_graph_full)

        fraction_infected_lattice_mean = np.mean(fraction_infected_lattice_full, axis=0)
        fraction_infected_random_graph_mean = np.mean(fraction_infected_random_graph_full, axis=0)
        fraction_infected_ws_graph_mean = np.mean(fraction_infected_ws_graph_full, axis=0)
        fraction_infected_ba_graph_mean = np.mean(fraction_infected_ba_graph_full, axis=0)

        plt.figure()
        plt.plot(fraction_infected_lattice_mean, label='Lattice')
        plt.plot(fraction_infected_random_graph_mean, label='Random Graph')
        plt.plot(fraction_infected_ws_graph_mean, label='WS Graph')
        plt.plot(fraction_infected_ba_graph_mean, label='BA Graph')
        plt.title('Fraction of infected with p={}'.format(p))
        plt.legend()
        plt.savefig('results/exercise_3/fraction_of_infected_{}.png'.format(p))

    Ps = np.linspace(0.1, 0.9, 24)
    results = pd.DataFrame(columns=['p', 'model', 'total_infected', 'infected_time', 'time_to_largest'])
    for p in Ps:
        for j in range(100):

            # Lattice
            population = PopulationOnLattice(p=p)
            infected_time = 0
            number_of_infected = []
            while population.infected:
                population.infect_neighbors()
                infected_time += 1
                number_of_infected.append(len(population.infected))
            time_to_largest = number_of_infected.index(max(number_of_infected))
            total_infected = len(population.removed)/(population.lattice.shape[0]*population.lattice.shape[1])
            results = results.append({'p': p,
                                      'model': 'lattice',
                                      'total_infected': total_infected,
                                      'infected_time': infected_time,
                                      'time_to_largest': time_to_largest}, ignore_index=True)

            # Random graph
            n = 100
            graph = nx.gnp_random_graph(n=n, p=0.3)
            population = PopulationOnGraph(graph=graph, p=p)
            infected_time = 0
            number_of_infected = []
            while population.infected:
                population.infect_neighbors()
                infected_time += 1
                number_of_infected.append(len(population.infected))
            time_to_largest = number_of_infected.index(max(number_of_infected))
            total_infected = len(population.removed) / n
            results = results.append({'p': p,
                                      'model': 'random_graph',
                                      'total_infected': total_infected,
                                      'infected_time': infected_time,
                                      'time_to_largest': time_to_largest}, ignore_index=True)

            # WS graph
            n = 100
            graph = nx.watts_strogatz_graph(n, 6, 0.4)
            population = PopulationOnGraph(graph=graph, p=p)
            infected_time = 0
            number_of_infected = []
            while population.infected:
                population.infect_neighbors()
                infected_time += 1
                number_of_infected.append(len(population.infected))
            time_to_largest = number_of_infected.index(max(number_of_infected))
            total_infected = len(population.removed) / n
            results = results.append({'p': p,
                                      'model': 'ws_graph',
                                      'total_infected': total_infected,
                                      'infected_time': infected_time,
                                      'time_to_largest': time_to_largest}, ignore_index=True)

            # BA graph
            n = 100
            graph = nx.barabasi_albert_graph(n, 4)
            population = PopulationOnGraph(graph=graph, p=p)
            infected_time = 0
            number_of_infected = []
            while population.infected:
                population.infect_neighbors()
                infected_time += 1
                number_of_infected.append(len(population.infected))
            time_to_largest = number_of_infected.index(max(number_of_infected))
            total_infected = len(population.removed) / n
            results = results.append({'p': p,
                                      'model': 'ba_graph',
                                      'total_infected': total_infected,
                                      'infected_time': infected_time,
                                      'time_to_largest': time_to_largest}, ignore_index=True)
    results.to_csv('results/exercise_3/results.csv')

