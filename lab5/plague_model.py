from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt


def si_model(y, t, b, beta, k):
    """SI model for numerical solving"""
    s, i = y
    return np.array([b*s - beta*i*s, beta*i*s - k*i])


if __name__ == '__main__':
    path = 'results/exercise_1/'
    b = 3
    beta = 3
    k = 3
    t = np.arange(0, 10, 0.01)
    for i0 in [0.1, 0.5, 1.3]:
        y0 = [1, i0]
        solution = odeint(si_model, y0, t, args=(b, beta, k))
        s = solution[:, 0]
        i = solution[:, 1]
        plt.figure(figsize=(10, 5))
        plt.subplot(121)
        plt.plot(t, s, linewidth=3, label='s(t)')
        plt.plot(t, i, label='i(t)')
        plt.xlabel('t')
        plt.legend()
        plt.title('Numerical solution')

        # Phase portrait
        n = 20
        x_s = np.linspace(min(s), max(s), n)
        x_i = np.linspace(min(i), max(i), n)

        X_s, X_i = np.meshgrid(x_s, x_i)
        dX_s, dX_i = si_model((X_s, X_i), 0, b=b, beta=beta, k=k)
        norm = np.hypot(dX_s, dX_i)
        dX_s /= norm
        dX_i /= norm
        plt.subplot(122)
        plt.plot(s, i)
        plt.quiver(X_s, X_i, dX_s, dX_i)
        plt.xlabel('s(t)')
        plt.ylabel('i(t)')
        plt.title('Phase portrait')

        plt.suptitle('Plague model s0={}, i0={}'.format(y0[0], y0[1]))
        name = 'plague_model_{}_{}.png'.format(y0[0], y0[1])
        plt.savefig(path + name)
