from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt


def sir_model(y, t, beta, r):
    """SIR model for numerical solving"""
    S, I, R = y
    return np.array([-beta*S*I, beta*S*I-r*I, r*I])


def sir_model_reduced(y, t, beta, r):
    """Reduced SIR model for numerical solving"""
    S, I = y
    return np.array([-beta*S*I, beta*S*I-r*I])


if __name__ == '__main__':
    path = 'results/exercise_2/'
    t = np.arange(0, 100, 0.1)
    N = 100
    r0s = []
    total_infected = []
    for beta in [0.01, 0.02, 0.05]:
        for r in [1, 1.5]:
            I0 = 1
            R0 = beta*N/r
            r0s.append(R0)
            y0 = [N, I0, 0]
            solution = odeint(sir_model, y0, t, args=(beta, r))
            S = solution[:, 0]
            I = solution[:, 1]
            R = solution[:, 2]
            total_infected.append(round(max(R)))
            plt.figure(figsize=(10, 5))
            plt.subplot(121)
            plt.plot(t, S, linewidth=3, label='s(t)')
            plt.plot(t, I, label='i(t)')
            plt.plot(t, R, label='r(t)')
            plt.legend()
            plt.xlim(0, 20)
            plt.title('Numerical solution')

            # Phase portrait
            n = 20
            x_s = np.linspace(min(S), max(S), n)
            x_i = np.linspace(min(I), max(I), n)

            X_s, X_i = np.meshgrid(x_s, x_i)
            dX_s, dX_i = sir_model_reduced((X_s, X_i), 0, beta=beta, r=r)
            norm = np.hypot(dX_s, dX_i)
            dX_s /= norm
            dX_i /= norm
            plt.subplot(122)
            plt.plot(S, I)
            plt.quiver(X_s, X_i, dX_s, dX_i)
            plt.xlabel('s(t)')
            plt.ylabel('i(t)')
            plt.title('Phase portrait')

            plt.suptitle('SIR model R0={}'.format(round(R0, 3)))
            name = 'SIR_model_{}.png'.format(round(R0, 1))
            plt.savefig(path + name)
    plt.figure()
    plt.bar(r0s, total_infected)
    plt.xlabel('R0')
    plt.ylabel('Total infected')
    plt.savefig('results/exercise_2/total_infected.png')
