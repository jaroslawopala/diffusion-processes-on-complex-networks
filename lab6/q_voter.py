import networkx as nx
import numpy as np
from multiprocessing import Pool, cpu_count


def calculate_magnetization(p):
    """Calculate magnetization for given p."""
    n_runs = 100
    n_time_steps = 1000
    n_agents = 100
    qs = [3, 4]
    graphs = [nx.complete_graph(n_agents),
              nx.barabasi_albert_graph(n_agents, 4),
              nx.watts_strogatz_graph(n_agents, 4, 0.01),
              nx.watts_strogatz_graph(n_agents, 4, 0.2)]
    magnetization = np.ones((len(qs), len(graphs), n_runs, n_time_steps + 1))
    for j, q in enumerate(qs):
        for g, graph in enumerate(graphs):
            # Runs
            for i in range(n_runs):
                # Time steps
                opinion = [1 for _ in range(n_agents)]
                for t in range(n_time_steps):
                    # Agents change opinions
                    for agent in np.random.choice(graph.nodes, n_agents):
                        neighbors = list(graph.neighbors(agent))
                        if np.random.random() > p and len(neighbors) > (q - 1):
                            # Check independence
                            neighbors_opinion = np.mean([opinion[node] for node in np.random.choice(neighbors, q)])
                            if np.abs(neighbors_opinion) == 1:
                                opinion[agent] = neighbors_opinion
                        else:
                            if np.random.random() > 0.5:
                                opinion[agent] *= (-1)
                    magnetization[j, g, i, t + 1] = np.mean(opinion)
    return magnetization


def main():
    """Main function"""
    ps = np.arange(0, 0.5, 0.02)
    with Pool(cpu_count()) as pool:
        magnetization = pool.map(calculate_magnetization, ps)

    np.save('results/magnetization.npy', magnetization)


if __name__ == '__main__':
    main()
