import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import imageio
from os import listdir


def random_step(graph, vertex):
    """ Return random step for vertex in graph. """
    return np.random.choice(list(graph.neighbors(vertex)), size=1)[0]


def visualize_step(graph, pos, v1, v2, n):
    """ Visualize random step.

    :param graph: graph
    :param pos: graph alignment for visualization
    :param v1: vertex from
    :param v2: vertex to
    :param n: number of step for saving purposes
    """
    edges = list(graph.edges())
    edge = (min(v1, v2), max(v1, v2))
    nx.draw_networkx_edges(graph, pos, edgelist=edges.remove(edge))
    nx.draw_networkx_edges(graph, pos, edgelist=[edge], edge_color='r')
    nodes = [x for x in list(graph.nodes()) if x not in [v1, v2]]
    nx.draw_networkx_nodes(graph, pos, nodelist=nodes)
    nx.draw_networkx_nodes(graph, pos, nodelist=[v1], node_color='red', label='Last node')
    nx.draw_networkx_nodes(graph, pos, nodelist=[v2], node_color='green', label='Current node')
    plt.legend(loc=1, markerscale=0.5, fancybox=True)
    plt.axis("off")
    plt.title('Random walk on a graph')
    plt.savefig('results/exercise_3/images/image_{}.png'.format(n + 100))
    plt.close()


def create_gif(path_to_images, path_to_result):
    """ Create gif from images.

    :param path_to_images: path to directory containing images
    :param path_to_result: path to directory to place the result
    """
    with imageio.get_writer(path_to_result, mode='I', fps=2) as writer:
        for filename in listdir(path_to_images):
            image = imageio.imread(path_to_images + filename)
            writer.append_data(image)


def calculate_hitting_times(graph, vertex):
    """ Calculate hitting times from vertex to other vertices in graph.

    :param graph: graph
    :param vertex: starting vertex
    :return: list of hitting times - index of each value is the same as node number
    """
    if not nx.is_directed(graph):
        lengths = [0]*graph.number_of_nodes()
        i = 0
        unvisited = list(graph.nodes())
        unvisited.remove(vertex)
        if nx.is_connected(graph):
            while unvisited:
                next_vertex = random_step(graph, vertex)
                i += 1
                if next_vertex in unvisited:
                    lengths[next_vertex] = i
                    unvisited.remove(next_vertex)
                vertex = next_vertex
        else:
            while i < 1000:
                next_vertex = random_step(graph, vertex)
                i += 1
                if next_vertex in unvisited:
                    lengths[next_vertex] = i
                    unvisited.remove(next_vertex)
                vertex = next_vertex
    else:
        lengths = [0]*graph.number_of_nodes()
        i = 0
        unvisited = list(graph.nodes())
        unvisited.remove(vertex)
        if nx.is_strongly_connected(graph):
            while unvisited:
                next_vertex = random_step(graph, vertex)
                i += 1
                if next_vertex in unvisited:
                    lengths[next_vertex] = i
                    unvisited.remove(next_vertex)
                vertex = next_vertex
        else:
            while i < 1000:
                i += 1
                if len(list(graph.neighbors(vertex))) > 0:
                    next_vertex = random_step(graph, vertex)
                    if next_vertex in unvisited:
                        lengths[next_vertex] = i
                        unvisited.remove(next_vertex)
                    vertex = next_vertex
                else:
                    break
    return lengths


if __name__ == '__main__':
    np.random.seed(42)

    # Generating graph
    graph = nx.gnp_random_graph(n=20, p=0.3)
    pos = nx.circular_layout(graph)
    visited = [0]
    n_steps = 20
    for i in range(n_steps):
        visited.append(random_step(graph, visited[-1]))
        visualize_step(graph, pos, visited[-2], visited[-1], i)

    # Creating gif
    create_gif('results/exercise_3/images/', 'results/exercise_3/random_walk_on_graph.gif')

    # Calculate hitting times for comparison
    M = 1000
    n = 100
    random_graph_times = np.zeros((M, n))
    watts_strogatz_times = np.zeros((M, n))
    barabasi_albert_times = np.zeros((M, n))
    random_graph = nx.gnp_random_graph(n=n, p=0.3)
    watts_strogatz = nx.watts_strogatz_graph(n=n, k=10, p=0.4)
    barabasi_albert = nx.barabasi_albert_graph(n=n, m=3)
    for i in range(M):
        random_graph_times[i, :] = calculate_hitting_times(random_graph, 0)
        watts_strogatz_times[i, :] = calculate_hitting_times(watts_strogatz, 0)
        barabasi_albert_times[i, :] = calculate_hitting_times(barabasi_albert, 0)
    random_graph_times_average = np.mean(random_graph_times, axis=0)
    watts_strogatz_times_average = np.mean(watts_strogatz_times, axis=0)
    barabasi_albert_times_average = np.mean(barabasi_albert_times, axis=0)
    plt.plot(random_graph_times_average, label='Random Graph n=100, p=0.3', color='k', alpha=0.8)
    plt.plot(watts_strogatz_times_average, label='Watts Strogatz Graph n=100, k=10, p=0.4', color='r', alpha=0.8)
    plt.plot(barabasi_albert_times_average, label='Barabasi Albert Graph n=100, m=3', color='g', alpha=0.8)
    plt.legend()
    plt.savefig('results/exercise_3/hitting_times_comparison.png')
    plt.close()

    # Calculate hitting times for directed graph
    M = 1000
    n = 100
    gnc_graph_times = np.zeros((M, n))
    gnc_graph = nx.gnc_graph(n=n)
    number_of_neighbors = [len(list(gnc_graph.neighbors(node))) for node in gnc_graph.nodes()]
    max_node = number_of_neighbors.index(max(number_of_neighbors))
    for i in range(M):
        gnc_graph_times[i, :] = calculate_hitting_times(gnc_graph, max_node)
    gnc_graph_times_average = np.mean(gnc_graph_times, axis=0)
    plt.plot(gnc_graph_times_average, label='GNC Graph')
    plt.legend()
    plt.savefig('results/exercise_3/hitting_times_directed_graph.png')
    plt.close()
