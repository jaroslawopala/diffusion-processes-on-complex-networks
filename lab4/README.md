# Assignment list 4

http://prac.im.pwr.wroc.pl/~szwabin/assets/diff/labs/l4.pdf

# Contents

- *results* directory contains gif and images for each exercise

# Exercise 1

Run *random_walk.py* to generate random walk on square lattice.

# Exercise 2

Run *pearson_random_walk.py* to generate Pearson random walk and calculate An and Bn.

# Exercise 3

Run *random_walk_on_graph.py* to generate random walk on graph and calculate average hitting times for 3 chosen graph structures.

# Summary

See *summary.html* or *summary.ipynb* for explanation of the results.