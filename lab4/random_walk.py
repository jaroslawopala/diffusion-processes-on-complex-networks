import numpy as np
import matplotlib.pyplot as plt
import imageio
from os import listdir


def create_gif(path_to_images, path_to_result):
    """ Create gif from images.

    :param path_to_images: path to directory containing images
    :param path_to_result: path to directory to place the result
    """
    with imageio.get_writer(path_to_result, mode='I', fps=2) as writer:
        for filename in listdir(path_to_images):
            image = imageio.imread(path_to_images + filename)
            writer.append_data(image)


class Agent:
    def __init__(self, x=0, y=0, lattice_x=(0, 10), lattice_y=(0, 10)):
        """ Initialize agent.

        :param x: initial position on x axis
        :param y: initial position on y axis
        :param lattice_x: lattice x borders
        :param lattice_y: lattice y borders
        """
        self.x = [x]
        self.y = [y]
        self.lattice_x = lattice_x
        self.lattice_y = lattice_y

    def move_up(self):
        """ Move agent up. """
        self.x.append(self.x[-1])
        self.y.append(self.y[-1] + 1)

    def move_down(self):
        """ Move agent down. """
        self.x.append(self.x[-1])
        self.y.append(self.y[-1] - 1)

    def move_left(self):
        """ Move agent left. """
        self.x.append(self.x[-1] - 1)
        self.y.append(self.y[-1])

    def move_right(self):
        """ Move agent right. """
        self.x.append(self.x[-1] + 1)
        self.y.append(self.y[-1])

    def random_move(self):
        """ Random move depending on the current position. """
        moves = []
        if self.y[-1] > self.lattice_y[0]:
            moves.append(self.move_down)
        if self.y[-1] < self.lattice_y[1]:
            moves.append(self.move_up)
        if self.x[-1] > self.lattice_x[0]:
            moves.append(self.move_left)
        if self.x[-1] < self.lattice_x[1]:
            moves.append(self.move_right)
        np.random.choice(moves, size=1)[0]()
        
    def draw_step(self):
        """ Draw the lattice with steps. """
        plt.subplots()
        plt.xlim(self.lattice_x[0] - 1, self.lattice_x[1] + 1)
        plt.ylim(self.lattice_y[0] - 1, self.lattice_y[1] + 1)
        plt.plot(self.x, self.y, color='blue', label='Random Walk', zorder=-1)
        plt.scatter(self.x[0], self.y[0], color='green', linewidths=3, label='Initial position')
        plt.scatter(self.x[-1], self.y[-1], color='red', linewidths=3, label='Actual position')
        plt.legend()
        plt.savefig('results/exercise_1/images/agent_{}.png'.format(99 + len(self.x)))
        plt.close()


if __name__ == '__main__':
    np.random.seed(42)
    agent = Agent()
    for i in range(100):
        agent.random_move()
        agent.draw_step()
    create_gif('results/exercise_1/images/', 'results/exercise_1/video.gif')
