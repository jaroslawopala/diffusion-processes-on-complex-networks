import numpy as np
import matplotlib.pyplot as plt
import imageio
from os import listdir


def create_gif(path_to_images, path_to_result):
    """ Create gif from images.

    :param path_to_images: path to directory containing images
    :param path_to_result: path to directory to place the result
    """
    with imageio.get_writer(path_to_result, mode='I', fps=5) as writer:
        for filename in listdir(path_to_images):
            image = imageio.imread(path_to_images + filename)
            writer.append_data(image)


class Agent:
    def __init__(self, x=0, y=0):
        """ Initialize agent in point (x, y). """
        self.x = [x]
        self.y = [y]

    def random_move(self):
        """ Random move of the agent. """
        angle = np.random.random() * 2*np.pi
        self.x.append(self.x[-1] + np.cos(angle))
        self.y.append(self.y[-1] + np.sin(angle))

    def draw_trajectory(self, n):
        """  Draw n-th trajectory of the agent. """
        plt.subplots()
        plt.xlim(min(self.x) - 5, max(self.x) + 5)
        plt.ylim(min(self.y) - 5, max(self.y) + 5)
        plt.plot(self.x, self.y, color='blue', zorder=-1, label='Pearson RW')
        plt.scatter(self.x[0], self.y[0], color='green', linewidths=3, label='Start point')
        plt.scatter(self.x[-1], self.y[-1], color='red', linewidths=3, label='End point')
        plt.legend()
        plt.savefig('results/exercise_2/trajectories/trajectory_{}.png'.format(n+1))
        plt.close()


if __name__ == '__main__':
    np.random.seed(42)

    # Trajectories visualization
    n_agents = 5
    n_steps = 1000
    for i in range(n_agents):
        agent = Agent()
        for j in range(n_steps):
            agent.random_move()
        agent.draw_trajectory(i)

    # Calculate An, Bn
    n_agents = 1000
    n_steps = 1000
    trajectories = np.empty((2, n_agents, n_steps+1))
    for i in range(n_agents):
        agent = Agent()
        for j in range(n_steps):
            agent.random_move()
        trajectories[0, i, :] = agent.x
        trajectories[1, i, :] = agent.y
    An = np.mean(trajectories[0] > 0, axis=1)
    Bn = np.mean((trajectories[0] > 0) * (trajectories[1] > 0), axis=1)
    plt.subplots(2, 1, figsize=(10, 10))
    plt.subplot(211)
    plt.hist(An, label='An', density=True)
    plt.title('An', fontsize=30)
    plt.subplot(212)
    plt.hist(Bn, color='orange', label='Bn', density=True)
    plt.title('Bn', fontsize=30)
    plt.savefig('results/exercise_2/An_Bn.png')
    plt.close()
    print('Mean of An: {}\nMean of Bn: {}'.format(round(An.mean(), 4), round(Bn.mean(), 4)))
