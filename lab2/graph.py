import warnings


class Vertex:
    def __init__(self, name, key):
        """Initialize empty graph."""
        self.name = name
        self.key = key
        self.neighbors = {}

    def __eq__(self, other):
        """Comparison operator to compare self with other vertex."""
        if self.key == other.key:
            return True
        return False


class Graph:
    def __init__(self, name = 'my_graph'):
        """Create an empty graph."""
        self.vertices = {}
        self.edges = {}
        self.name = name

    def addVertex(self, vert):
        """Add vertex to the graph.

        :param vert: vertex to add
        """
        if vert.key not in self.vertices:
            self.vertices[vert.key] = vert
        else:
            warnings.warn("Vertex was not added. Key {} already taken.".format(vert.key))

    def addVerticesFromList(self, vertList):
        """Add vertices from a list.

        :param vertList: list of vertices to add
        """
        [self.addVertex(vert) for vert in vertList]

    def addEdge(self, fromVert, toVert, weight = 1):
        """Add edge to the graph.

        :param fromVert: starting vertex
        :param toVert: ending vertex
        """
        if weight <= 0:
            raise ValueError("Weight has to be positive.")
        if fromVert not in self.vertices.values():
            self.addVertex(fromVert)
        if toVert not in self.vertices.values():
            self.addVertex(toVert)
        self.vertices[fromVert.key].neighbors[toVert.key] = weight
        self.vertices[toVert.key].neighbors[fromVert.key] = weight
        if (fromVert.key, toVert.key) not in self.edges.keys() and (toVert.key, fromVert.key) not in self.edges.keys():
            self.edges[(fromVert.key, toVert.key)] = weight

    def addEdgesFromList(self, edgeList):
        """Add edges from a list.

        :param edgeList: list of edges, a list of tuples
        """
        for edge in edgeList:
            if len(edge) < 3:
                self.addEdge(edge[0], edge[1])
            else:
                self.addEdge(edge[0], edge[1], edge[2])

    def getVertices(self):
        """Get vertices of the graph."""
        return list(self.vertices.keys())

    def getEdges(self):
        """Get edges of the graph."""
        edges = []
        for k in self.vertices:
            edges.append((k, self.vertices[k].neighbors))
        return edges

    def getNeighbors(self, vertKey):
        """Get neighbors of given vertex.

        :param vertKey: vertex to get neighbors of
        :return: list of neighbors
        """
        return self.vertices[vertKey].neighbors

    def __contains__(self, vert):
        """Check if vertex is present in the graph."""
        return vert.key in self.vertices

    def saveGraph(self):
        """Save dot representation of the graph to a text file."""
        with open(str(self.name) + ".txt", 'w') as dot:
            dot.write("graph " + self.name + "{\n")
            for edge in self.edges:
                dot.write(str(self.vertices[edge[0]].name) + " -- " + str(self.vertices[edge[1]].name) + " [label = " + str(self.edges[edge]) + "]\n")
            dot.write("}")
        print("Graph saved succesfully!")

    def __str__(self):
        """str() representation of graph."""
        return "Vertices: " + str(list(self.vertices.keys())) + ", Edges: " + str(self.getEdges())

    def getShortestPaths(self, fromVert):
        """Calculate shortest paths from fromVert to every other vertex."""
        unvisited = list(self.vertices.keys())
        distances = {node: float("inf") for node in unvisited}
        distances[fromVert] = 0
        current = fromVert
        tentative = distances.copy()
        while unvisited:
            for n in self.vertices[current].neighbors:
                if n in unvisited:
                    distances[n] = min(distances[current] + self.vertices[current].neighbors[n], distances[n])
                    tentative[n] = min(distances[current] + self.vertices[current].neighbors[n], distances[n])
            del tentative[current]
            unvisited.remove(current)
            if tentative:
                current = min(tentative, key=tentative.get)
        return distances


def evaluate_graph(G, n, k):
    """Check if the functions work properly."""
    print("\nVertices of graph " + str(G.name) + ": " + str(G.getVertices()))
    print("\nEdges displayed in form (fromVert, {toVert: weight}).")
    print("Edges of graph " + str(G.name) + ": " + str(G.getEdges()))
    print("\nNeighbors displayed in form {neighbor: weight}.")
    print("Neighbors of " + str(n) + ": " + str(G.getNeighbors(n)))
    print("\nstr representation of graph " + str(G.name) + ": " + str(G))
    shortest = G.getShortestPaths(k)
    print("\nFind shortest paths from: " + str(G.vertices[k].name))
    for i in shortest:
        print(G.vertices[i].name + ": " + str(shortest[i]))
    G.saveGraph()


if __name__ == '__main__':
    import csv
    G = Graph("social_network")
    people = ["Alice", "Bob", "Gail", "Irene", "Carl", "Harry", "Jen", "David", "Ernst", "Frank"]
    keys = {people[i]: i for i in range(len(people))}
    print("People are encoded as: " + str(keys))

    print("Adding nodes from social network (assignment 1)...")
    with open("nodes.csv", newline='') as nodes:
        nodes_reader = csv.reader(nodes, delimiter = ',')
        for node in nodes_reader:
            G.addEdge(Vertex(node[0], keys[node[0]]), Vertex(node[1], keys[node[1]]))
    print("Nodes added!")

    evaluate_graph(G, 1, 8)

    G.name = "social_network_with_me"
    G.addVerticesFromList([Vertex("Jarek", 10),(Vertex("Jareczek", 11))])
    G.addEdgesFromList([(Vertex("Jarek", 10), G.vertices[7], 4), (Vertex("Jareczek", 11), G.vertices[3], 2)])

    evaluate_graph(G, 1, 8)
