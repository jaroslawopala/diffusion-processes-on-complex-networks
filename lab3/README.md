# Assignment list 3

http://prac.im.pwr.wroc.pl/~szwabin/assets/diff/labs/l3.pdf

# Contents

- *results* directory contains images of graphs and nodes degree distributions and *.csv* files
- *graphs* directory contains saved graphs with N=2000 (ex.1) and social network (ex.2)

# Exercise 1

1) Run graphs.py to generate graphs with N=20 and N=2000 nodes

2) Examples of graphs with N=20 are stored in *results* directory.

3) Graphs with N=2000 were generated, saved to files in *graphs* directory.

4) Graphs properties were saved in *properties.csv* file in *results* directory.

5) Nodes degree distributions were plotted in *results_explanation.html* or *results_explanation.html* and saved in *results* directory.

# Exercise 2

1) Run *exercise_2.py* to fetch data from livejournal.com

2) See *exercise_2_analysis.ipynb* or *exercise_2_analysis.html* for required statistics

3) See *nodes_edges.csv*, *degree_centrality.csv*, *betweenness_centrality.csv* in the *results* directory for saved statistics