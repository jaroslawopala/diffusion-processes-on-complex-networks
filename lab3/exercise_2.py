import networkx as nx
import urllib.request


def fetch_friends(user):
    link = 'http://www.livejournal.com/misc/fdata.bml?user='
    response = urllib.request.urlopen(link + user)
    data = response.readlines()
    friends = []
    i = 1
    while i < len(data):
        if data[i][0] == 60:
            friends.append(data[i].decode()[2:-1])
        i += 1
    return friends


if __name__ == '__main__':
    G = nx.Graph()
    user = 'valerois'
    friends = {}
    friends[user] = fetch_friends(user)
    for friend in friends[user]:
        print(friend)
        friends[friend] = fetch_friends(friend)
    for friend in friends:
        print(friend)
        for f in friends[friend]:
            G.add_edge(friend, f)

    nx.write_gpickle(G, 'graphs/valerois.gpickle')
    print('Graph saved to file!')
