import networkx as nx
import numpy as np
import matplotlib.pyplot as plt


def generate_random_graph(n, p):
    """Generate random graph G(n,p)."""
    graph = nx.Graph()
    for i in range(n):
        graph.add_node(i)
    for i in range(n):
        for j in range(i, n):
            if i != j:
                if np.random.random() < p:
                    graph.add_edge(i, j)
    return graph


def generate_watts_strogatz_graph(n, k, p):
    """Generate Watts Strogatz graph with parameters n, k, p."""
    if k <= np.log(n) or k > n or k % 2 != 0:
        raise ValueError("Invalid k")
    graph = nx.Graph()
    for i in range(n):
        graph.add_node(i)
    for i in range(n):
        for j in range(n):
            if 0 < (abs(i - j) % (n - 1 - k/2)) <= k/2:
                graph.add_edge(i, j)
    for i in range(n):
        for j in range(i + 1, i + k//2 + 1):
            if np.random.random() <= p:
                graph.remove_edge(i, j % n)
                running = True
                while running:
                    new_edge = np.random.randint(0, n-1)
                    if not graph.has_edge(i, new_edge):
                        if i != new_edge:
                            graph.add_edge(i, new_edge)
                            running = False
    return graph


def generate_barabasi_albert_graph(n, m, m0=None):
    """Generate Barabasi Albert graph with parameters n, m, m0."""
    graph = nx.Graph()
    if not m0:
        m0 = m
    for i in range(m0):
        for j in range(i, m0):
            if i != j:
                graph.add_edge(i, j)
    degrees = {node: degree for node, degree in graph.degree}
    for i in range(m0, n):
        probs = np.cumsum(np.fromiter(degrees.values(), dtype=float)/(2*graph.number_of_edges()))
        j = m
        degrees[i] = 0
        while j:
            k = np.sum(probs < np.random.random())
            if i != k and not graph.has_edge(i, k):
                graph.add_edge(i, k)
                degrees[i] += 1
                degrees[k] += 1
                j -= 1
    return graph


def save_graph_plot(graph, name, circular=True, **kwargs):
    """Save graph plot to png file."""
    plt.subplots()
    title = name
    for key, value in kwargs.items():
        title += ' ' + str(key) + '=' + str(value)
    if circular:
        nx.draw_circular(graph)
    else:
        nx.draw(graph)
    plt.title(title)
    plt.savefig('results/' + title + '.png')
    print(name + " saved successfully!")


def evaluate_graph_stats(name, graph_stats, theor=True):
    """Print basic graph statistics.

    :param name: graph name, str
    :param graph_stats: graph statistics, dict
    :param theor: does graph_stats include theoretical values, bool
    :return:
    """
    print('\nEvaluation of {}'.format(name))
    print("Number of nodes: {}".format(graph_stats['nodes']))
    if theor:
        print("Theoretical number of edges: {}".format(graph_stats['theoretical_edges']))
    print("Number of edges: {}".format(graph_stats['edges']))
    if theor:
        print("Theoretical average degree: {}".format(graph_stats['theoretical_average_degree']))
    print("Average degree: {}".format(graph_stats['average_degree']))
    if theor:
        print("Theoretical degree variance: {}".format(graph_stats['theoretical_degree_variance']))
    print("Degree variance: {}".format(graph_stats['degree_variance']))


if __name__ == '__main__':
    np.random.seed(42)

    # Graph plots generating, N = 20
    # Random Graph
    n = 20
    P = [0.2, 0.5, 0.8]
    for p in P:
        save_graph_plot(generate_random_graph(n, p), 'Random_graph', circular=True, n=n, p=p)

    # Watts-Strogatz Graph
    n = 20
    K = [4, 6]
    P = [0.3, 0.7]
    for k in K:
        for p in P:
            save_graph_plot(generate_watts_strogatz_graph(n, k, p), 'Watts-Strogatz_graph', circular=True, n=n, k=k, p=p)

    # Barabasi-Albert Graph
    n = 20
    M = [2, 3, 4]
    for m in M:
        save_graph_plot(generate_barabasi_albert_graph(n, m), 'Barabasi-Albert_graph', circular=False, n=n, m=m)

    # Graph characteristics, N = 2000
    # Random Graph
    n = 2000
    p = 0.4
    random_graph = generate_random_graph(n, p)
    random_graph_stats = {'nodes': random_graph.number_of_nodes(),
                          'theoretical_edges': p*n*(n-1)/2,
                          'edges': random_graph.number_of_edges(),
                          'theoretical_average_degree': p*(n-1),
                          'average_degree': np.mean([value for key, value in random_graph.degree]),
                          'theoretical_degree_variance': p*(1-p)*(n-1),
                          'degree_variance': np.var([value for key, value in random_graph.degree])}
    evaluate_graph_stats('random graph', random_graph_stats)
    nx.write_gpickle(random_graph, 'graphs/random_graph.gpickle')
    print('Graph saved to file!')

    # Watts Strogatz Graph
    n = 2000
    k = 20
    p = 0.5
    watts_graph = generate_watts_strogatz_graph(n, k, p)
    watts_graph_stats = {'nodes': watts_graph.number_of_nodes(),
                         'edges': watts_graph.number_of_edges(),
                         'average_degree': np.mean([value for key, value in watts_graph.degree]),
                         'degree_variance': np.var([value for key, value in watts_graph.degree])}
    evaluate_graph_stats('watts graph', watts_graph_stats, False)
    nx.write_gpickle(watts_graph, 'graphs/watts_graph.gpickle')
    print('Graph saved to file!')
    watts_graph_poisson = generate_watts_strogatz_graph(n, k, 1)
    nx.write_gpickle(watts_graph_poisson, 'graphs/watts_graph_poisson.gpickle')
    print('Graph saved to file!')

    # Barabasi Graph
    n = 2000
    m = 3
    barabasi_graph = generate_barabasi_albert_graph(n, m)
    barabasi_graph_stats = {'nodes': barabasi_graph.number_of_nodes(),
                            'edges': barabasi_graph.number_of_edges(),
                            'average_degree': np.mean([value for key, value in barabasi_graph.degree]),
                            'degree_variance': np.var([value for key, value in barabasi_graph.degree])}
    evaluate_graph_stats('barabasi graph', barabasi_graph_stats, False)
    nx.write_gpickle(barabasi_graph, 'graphs/barabasi_graph.gpickle')
    print('Graph saved to file!')

    with open('results/properties.csv', 'w') as file:
        for key, value in random_graph_stats.items():
            file.write('random_graph,' + str(key) + ',' + str(value) + '\n')
        for key, value in watts_graph_stats.items():
            file.write('watts_graph,' + str(key) + ',' + str(value) + '\n')
        for key, value in barabasi_graph_stats.items():
            file.write('barabasi_graph,' + str(key) + ',' + str(value) + '\n')
    print('Graphs properties saved to csv!')
